---
title: Rapport du Projet Gumstix
subtitle: Informatique embarquée
# institute: Université de Cergy-Pontoise
author:
- Julien Abadji
- Vincent Monot
- Louis Desportes
date:
documentclass: scrreprt
lang: fr

toc: false
# fontsize: 11pt
# geometry: margin=1.27cm
---
## Compilation
Le projet peut être compilé avec la commande `make` à la racine. Lancé avec `udp_client_gum.out` coté gumstix, `udp_server.out` coté serveur

## Certification
La certification s'effectue en faisant la différence entre paquets envoyés et reçus et la théorie:

- pertes calcul = nombre théorique - nombre envoyés
- pertes WiFi = nombre envoyés - nombre reçus
- pertes Totales = nombre théorique - nombre reçus

Quelques modifications ont été apportées au projet depuis la dernière certification. Les résultats peuvent varier.

![Données envoyés(bleues) vs reçues(vert) — échelle absolue](cote_a_cote.png)

![Pertes totales(bleues) vs WiFi(vert) — ratio](ratio.png)
