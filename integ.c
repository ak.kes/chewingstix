#include <stdio.h>
#include <stdlib.h>

int main() {
  float acc[10] = {0, 0, 2, 4, 6, 4, 2, 1, 0, 0};
  int step = 2;
  int i;

  for (i=0; i+step<10; i+=step){
    //Just trying things, don't be afraid.
    printf("Intégration entre %f et %f\n", acc[i], acc[i+step]);
    printf("%f\n", step*((acc[i]+acc[i+step]))/2);
  }
}
