GCC=gcc
GUM=arm-linux-gnueabi-gcc
OPTIONS=-Wall -pedantic -g
FAKEFLAG=-D __FAKE_I2C
all: gcc gum

gcc: udp_client.c udp_server.c
	$(GCC) $(OPTIONS) -o host/udp_server.out udp_server.c

gcc-fake: udp_client.c udp_server.c
	$(GCC) $(OPTIONS) $(FAKEFLAG) -o host/udp_server.out udp_server.c

gum-fake: udp_client.c udp_server.c
		$(GUM) $(OPTIONS) $(FAKEFLAG) -o host/udp_server.out udp_server.c

gum: udp_client.c udp_server.c
	$(GUM) $(OPTIONS) -o gumstix/udp_client_gum.out udp_client.c i2c-tools-3.1.0/tools/util.c i2c-tools-3.1.0/tools/i2cbusses.c

bench: bench.c
	$(GUM) $(OPTIONS) -o gumstix/bench.out bench.c i2c-tools-3.1.0/tools/util.c i2c-tools-3.1.0/tools/i2cbusses.c
clean:
	rm -rf host/*.out gumstix/*.out
