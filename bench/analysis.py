import numpy as np
import matplotlib.pyplot as plt
import csv
import operator


def load_files():
    data = []

    for i in range(500, 10000, 100):
        with open("gumstix/result_" + str(i) + ".log") as f:
            # table = csv.reader(f, delimiter=' ')
            num_lines = sum(1 for line in f)
            data.append(num_lines)
            # data.append(table.line_num)
    return data


def packets_received():
    received_packets = [0] * 95
    with open("host/result.log") as f:
        for line in f:
            # print(line.split(" ")[0])
            freq = int(int(line.split(" ")[0])/100-5)
            # print(str(freq))
            received_packets[freq] = received_packets[freq] + 1

    return received_packets

data = load_files()
print(str(len(data)))
udp_loss = packets_received()
print(str(len(udp_loss)))
plt.axis([500, 10000, 0, 1.1])
n = np.arange(500, 10000, 100)
# plt.plot(n, data)
# plt.plot(n, udp_loss)
ratio_wireless = list(map(operator.truediv, udp_loss, data))
ratio_total = list(map(operator.truediv, udp_loss, (100,)*95))
print(str(ratio_total))
plt.plot(n, ratio_total)
plt.plot(n, ratio_wireless)
plt.show()
